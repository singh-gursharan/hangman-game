package main

import (
	"bufio"
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
)

func main() {
	file, err := os.Open("words.txt")

	if err != nil {
		fmt.Printf("failed opening file: %s", err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)
	var words []string

	for scanner.Scan() {
		words = append(words, scanner.Text())
	}
	file.Close()
	rand.Seed(time.Now().UnixNano())
	index := rand.Intn(len(words))
	word := strings.ToLower(words[index])
	result := guessWord(word)
	fmt.Println(result)
	fmt.Printf("==========================\nThe word was '%s' \n=====================\n", word)
}

func guessWord(word string) string {
	wordArr := strings.Split(word, "")
	wordMade := make([]string, len(word))
	misses := make([]string, 0, 5)
	for i := 0; i < len(wordMade); i++ {
		wordMade[i] = "_"
	}
	var guess string
	var trueGuesses int = 0
	var missesASCII [128]bool
	for len(misses) < 5 {
		fmt.Printf("Word: %s ", strings.Join(wordMade, " "))
		fmt.Println()
		fmt.Printf("Guess:")
		fmt.Scanln(&guess)
		guess = strings.ToLower(guess)
		var found bool
		if trueGuesses, found = fill(guess, wordArr, wordMade, trueGuesses); found {
			if trueGuesses == len(wordMade) {
				return "You Win"
			}
		} else {
			if !missesASCII[[]byte(guess)[0]] {
				misses = append(misses, guess)
				missesASCII[[]byte(guess)[0]] = true
			}
		}
		fmt.Printf("Misses: %s", strings.Join(misses, ","))
		fmt.Println()
	}
	return "You lost"
}

func fill(letter string, wordArr []string, wordMade []string, trueGuesses int) (int, bool) {
	foundFlag := false
	for i, e := range wordArr {
		if letter == e {
			foundFlag = true
			wordMade[i] = e
			trueGuesses++
		}
	}
	if !foundFlag {
		return trueGuesses, false
	}
	return trueGuesses, true
}
